import os
import pickle

import pandas as pd

from annotated_dataframe.annotation import Annotation, SUPPORTED_COLUMNS
from annotated_dataframe.utils import make_dataframe, eval_pd_formula

dir_data = os.path.join(os.path.split(__file__)[0], 'data')


def test_formula_supported():
    assert 'formula' in SUPPORTED_COLUMNS


def test_formula_computation():
    annotation = Annotation(os.path.join(dir_data, 'annotation2.csv'))
    data = make_dataframe(os.path.join(dir_data, 'data2.csv'))
    ref = pickle.load(open(os.path.join(dir_data, 'ref2.pkl'), 'rb'))
    res = annotation.transform(data)
    pd.testing.assert_frame_equal(res, ref, check_like=True)



def test_formula_with_dtype():
    annotation = Annotation(os.path.join(dir_data, 'annotation3.csv'))
    data = make_dataframe(os.path.join(dir_data, 'data3.csv'))
    res = annotation.transform(data)
    assert res.dtypes['m1'] == 'int'
    assert res.dtypes['m2'] == 'float'


