import os

import pandas as pd

from annotated_dataframe import AnnotatedDataFrame
from annotated_dataframe.annotation import ROLES, Annotation
from annotated_dataframe.utils import make_dataframe

dir_data = os.path.join(os.path.split(__file__)[0], 'data')


def test_simple():
    annotation = make_dataframe(
        """source_name
        x
        y
        z"""
    )
    data = make_dataframe(
        """
            x, y, z
            1, 2, 2
        """
    )
    _ = AnnotatedDataFrame(data, annotation)


def test_column_name_conversion():
    annotation = make_dataframe(
        """
        name, source_name
        x, source x
        y, source y
        """
    )
    data = make_dataframe(
        """
        source x, source y
            1, 2
        """
    )
    adf = AnnotatedDataFrame(data, annotation)
    for n in annotation['name']:
        assert n in adf.columns
    for n in annotation.source_name:
        assert n not in adf.columns


def _adf_with_annotations(return_source_tables=False) -> AnnotatedDataFrame:
    annotation = make_dataframe("""
        source_name, role
        a,meta
        b,meta
        c,feature
        d,target
        e,feature
        f,skip
        g,
        """)
    data = make_dataframe(("""
            a,  b,  c,  d,  e,  f,  g
            a0, b0, c0, d0, e0, f0, g0
            a1, b1, c1, d1, e1, f1, g1
        """))
    adf = AnnotatedDataFrame(data, annotation)
    if return_source_tables:
        return adf, data, annotation
    else:
        return adf


def test_access_attribute_by_role():
    adf = _adf_with_annotations()
    ref = {
        'meta': pd.DataFrame({'a': ['a0', 'a1'], 'b': ['b0', 'b1']}),
        'feature': pd.DataFrame({'c': ['c0', 'c1'], 'e': ['e0', 'e1']}),
        'target': pd.DataFrame({'d': ['d0', 'd1']}),
        'skip': pd.DataFrame({'f': ['f0', 'f1']}),
        'undef_role': pd.DataFrame({'g': ['g0', 'g1']})
    }
    supported_roles = [r for r in ROLES if r] + ['undef_role']
    errs = []
    for role in supported_roles:
        try:
            result = getattr(adf, role)
        except AttributeError:
            errs.append(role)
        else:
            pd.testing.assert_frame_equal(result, ref[role], check_like=True)
    if errs:
        raise AttributeError(f'{repr(errs)} attribute(s) not found')


def test_trim_undef_true_by_default():
    adf = _adf_with_annotations()
    ref = pd.DataFrame(adf)[['a', 'b', 'c', 'd', 'e']]
    trimmed = adf.trimmed()
    pd.testing.assert_frame_equal(ref, trimmed, check_like=True)


def test_columns_accessible_as_attributes():
    adf, data, annotation = _adf_with_annotations(return_source_tables=True)
    for c in data.columns:
        ref = data[c]
        res = getattr(adf, c)
        pd.testing.assert_series_equal(ref, res)


def test_columns_accessible_as_items():
    adf, data, annotation = _adf_with_annotations(return_source_tables=True)
    for c in data.columns:
        ref = data[c]
        res = adf[c]
        pd.testing.assert_series_equal(ref, res)


def test_trim_undef_true_explicit():
    adf = _adf_with_annotations()
    ref = pd.DataFrame(adf)[['a', 'b', 'c', 'd', 'e']]
    trimmed = adf.trimmed(including_undef=True)
    pd.testing.assert_frame_equal(ref, trimmed, check_like=True)


def test_trim_undef_false():
    adf = _adf_with_annotations()
    ref = pd.DataFrame(adf)[['a', 'b', 'c', 'd', 'e', 'g']]
    trimmed = adf.trimmed(including_undef=False)
    pd.testing.assert_frame_equal(ref, trimmed, check_like=True)


def test_constructor_doesnt_change_the_parameter():
    annotation = make_dataframe("""
        source_name,name,role
        x source,x,feature
        y source,y,skip
    """)
    data = pd.DataFrame({'x source': [1,2,3], 'y source': [1, 2, 3]})
    before = data.copy()
    _ = AnnotatedDataFrame(data, annotation)
    pd.testing.assert_frame_equal(before, data)


def test_constructor_accepts_annotation_object():
    annotation = make_dataframe("""
            source_name,name,role
            x source,x,feature
            y source,y,skip
        """)
    annotation = Annotation(annotation)
    data = pd.DataFrame({'x source': [1, 2, 3], 'y source': [1, 2, 3]})
    AnnotatedDataFrame(data=data, annotation=annotation)