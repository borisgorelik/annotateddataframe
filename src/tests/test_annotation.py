import os

import numpy as np
import pandas as pd
import pytest

from annotated_dataframe.annotation import Annotation, REQUIRED_COLUMNS, SUPPORTED_COLUMNS
from annotated_dataframe.utils import make_dataframe

dir_data = os.path.join(os.path.split(__file__)[0], 'data')


def test_create_empty():
    Annotation(pd.DataFrame(columns=REQUIRED_COLUMNS))


def test_annotation_simple():
    annotation = make_dataframe("""
        name,source_name, role, others_threshold_percent, dtype,note
        x   , skip,                       ,         , note
    """)
    Annotation(annotation)


def test_annotation_missing_required_column():
    annotation = pd.DataFrame(columns='name,source_name,role,others_threshold_percent,dtype,note'.split(','))
    for col in REQUIRED_COLUMNS:
        tmp = annotation.drop(col, axis=1)
        with pytest.raises(ValueError):
            Annotation(tmp)


def test_no_empty_names():
    annotation = pd.DataFrame({'source_name': ['', 'x']})
    with pytest.raises(ValueError):
        Annotation(annotation)


def test_annotation_unsupported_columns():
    columns = 'name,source_name,role,others_threshold_percent,dtype,note'.split(',')
    _ = Annotation(pd.DataFrame(columns=columns))  # should be OK
    columns.append("UNSUPPORTED")
    annotation = pd.DataFrame(columns=columns)
    with pytest.warns(RuntimeWarning):
        Annotation(annotation)


def test_unique_names():
    annotation = pd.DataFrame({'source_name': ['x', 'y', 'x']})
    with pytest.raises(ValueError):
        Annotation(annotation)


def test_blank_names():
    annotation = pd.DataFrame({'source_name': ['x', '', 'z']})
    with pytest.raises(ValueError):
        Annotation(annotation)


def test_column_name_conversion():
    annotation = make_dataframe(
        """
        name, source_name
        x, source x
        y, source y
        """
    )
    annotation = Annotation(annotation)
    data = make_dataframe(
        """
        source x, source y
            1, 2
        """
    )

    transformed = annotation.transform(data)
    for n in annotation.annotation['name']:
        assert n in transformed.columns
    for n in annotation.annotation.source_name:
        assert n not in transformed.columns


def test_respect_dtype():
    annotation = make_dataframe("""
        source_name, dtype
        x, str
        y, float
        z, int
    """)
    annotation = Annotation(annotation)
    data = pd.DataFrame({'x': [1, 2, 3], 'y': [1.2, 2, 3], 'z': [1.1, 2.2, 3]})
    transformed = annotation.transform(data)
    assert transformed.dtypes['x'] == 'object'
    assert transformed.dtypes['y'] == 'float'
    assert transformed.dtypes['z'] == 'int'


def test_respect_dtype_with_missing_values():
    annotation = make_dataframe("""
        source_name, dtype, na_value
        x, str
        y, float
        z, int, 0
    """)
    annotation = Annotation(annotation)
    data = pd.DataFrame({'x': [1, None, 3], 'y': [None, 2, 3], 'z': [1.1, 2.2, None]})
    transformed = annotation.transform(data)
    assert transformed.dtypes['x'] == 'object'
    assert transformed.dtypes['y'] == 'float'
    assert transformed.dtypes['z'] == 'int'


def test_respect_dtype_missing_values():
    annotation = make_dataframe("""
            source_name, dtype
            x, str
        """)
    annotation = Annotation(annotation)
    data = make_dataframe("""
        x, y
        123,a
         , b
        345, c
    """)
    transformed = annotation.transform(data)
    assert transformed.x.dtype == 'object'
    x = transformed.x
    assert not x.str.endswith('.0').any()


def test_category_dtype():
    annotation = make_dataframe("""
        source_name, dtype
        x, category
        y, category
        z, category
    """)
    annotation = Annotation(annotation)
    data = pd.DataFrame({'x': ['a', 'b', 'c'], 'y': [1, 2, 3], 'z': [1.1, 2.2, 3]})
    transformed = annotation.transform(data)
    ref = {'x': pd.CategoricalDtype(categories=['a', 'b', 'c'], ordered=False),
           'y': pd.CategoricalDtype(categories=[1, 2, 3], ordered=False),
           'z': pd.CategoricalDtype(categories=[1.1, 2.2, 3.0], ordered=False)
           }
    dtypes = transformed.dtypes.to_dict()
    assert dtypes == ref


def test_read_time_dtypes_respected():
    annotation = make_dataframe(
        """source_name,dtype
        x,int
        y,str
        z,
        """
    )
    annotation = Annotation(annotation)
    dtypes = annotation.dtypes(source_names=True)
    data = make_dataframe(
        """
            x, y, z
            1.0, 2.0, 2.0
        """,
        dtypes=dtypes
    )
    ref = {'x': np.dtype('int64'), 'y': np.dtype('O'), 'z': np.dtype('float64')}
    assert ref == dict(data.dtypes)


def test_read_time_dtypes_respected_csv_and_excel():
    for extension in {'csv', 'xlsx'}:
        annotation = make_dataframe(os.path.join(dir_data, f'annotation1.{extension}'))
        annotation = Annotation(annotation)
        dtypes = annotation.dtypes(source_names=True)
        data = make_dataframe(
            os.path.join(dir_data, f'data1.{extension}'),
            dtypes=dtypes
        )
        at_least_once = False
        for col, typ in data.dtypes.iteritems():
            typ = str(typ)
            if typ == 'object':
                typ = 'str'
            if col in dtypes:
                assert dtypes[col] == typ
                at_least_once = True
        assert at_least_once


def test_formula_or_source_name():
    annotation = """name,source_name,formula
        x,was x, int(x)
        """
    with pytest.raises(ValueError):
        Annotation(annotation)


def test_default_name():
    annotation = Annotation("""
    source_name
    x
    y
    z
    """)
    assert (annotation.annotation['name'] == annotation.annotation['source_name']).all()


def test_empty_name_column():
    annotation = Annotation("""
        source_name,name
        x
        y
        """)
    assert (annotation.annotation['name'] == annotation.annotation['source_name']).all()


def test_partial_name_column():
    anno = make_dataframe("""
        source_name,name
        x,was_x
        y
        """)
    expected = ['was_x', 'y']
    annotation = Annotation(anno)
    observed = annotation.annotation['name']
    assert np.equal(expected, observed).all()


def test_empty_formula_column():
    annotation = Annotation("""
        source_name,formula
        x
        y
        """)
    assert (annotation['formula'] == '').all()


def test_no_source_name_when_formula_exists():
    with pytest.raises(ValueError):
        Annotation("""
            source_name,formula
            x
            y
            z
            formula,np.round(@df.x)
            """)


def test_empty_role_column():
    annotation = Annotation("""
        source_name,role
        x
        y
        """)
    assert (annotation['role'] == '').all()


def test_unsupported_role():
    with pytest.raises(ValueError):
        Annotation("""
                source_name,role
                x,target
                y,usupported_crap
                """)


def test_supported_roles():
    Annotation("""
            source_name,role
            x,meta
            y,target
            z,skip
            u,feature
            w,feature
            """)


def test_others_dtype():
    anno = make_dataframe("""
    source_name,others_threshold_percent
    x,
    y,1.0
    """)
    annotation = Annotation(anno)
    assert annotation.annotation.select_dtypes(np.number).columns.to_list() == ['others_threshold_percent']


def test_others_only_numeric_accepted():
    with pytest.raises(ValueError):
        Annotation("""
        source_name,others_threshold_percent
        x,1.1
        y,one
        """)


def test_others_illegal_values():
    for illegal in (-1.1, 101.0):
        with pytest.raises(ValueError):
            Annotation(f"""
            source_name,others_threshold_percent
            x,1.1
            y,{illegal}
            """)


def test_supported_columns_accessible_as_attrubutes():
    annotation = Annotation("""
    source_name
    x
    y
    """)
    errs = []
    for c in SUPPORTED_COLUMNS:
        try:
            getattr(annotation, c)
        except AttributeError:
            errs.append(c)
    if errs:
        raise AssertionError(f'Missing attributes {repr(errs)}')


def _make_annotation_data_for_stripping():
    annotation = make_dataframe("""
        source_name
        x
        """)
    data = pd.DataFrame(
        {'x': ['no', ' leading', 'trailing', ' both '],
         'y': [1, 2, 3, 4]}
    )
    return annotation, data


def test_strings_stripped_by_default():
    annotation, data = _make_annotation_data_for_stripping()
    annotation = Annotation(annotation)
    transformed = annotation.transform(data)
    pd.testing.assert_series_equal(data['x'].str.strip(), transformed['x'])


def test_strings_stripped_explicitly():
    annotation, data = _make_annotation_data_for_stripping()
    annotation = Annotation(annotation, strip_strings=True)
    transformed = annotation.transform(data)
    pd.testing.assert_series_equal(data['x'].str.strip(), transformed['x'])


def test_strings_not_stripped_explicitly():
    annotation, data = _make_annotation_data_for_stripping()
    annotation = Annotation(annotation, strip_strings=False)
    transformed = annotation.transform(data)
    pd.testing.assert_series_equal(data['x'], transformed['x'])


def test_transform_doesnt_change_the_original_data():
    annotation = Annotation("""
        source_name,name,role
        x source,x,feature
        y source,y,skip
    """)
    data = pd.DataFrame({'x source': [1, 2, 3], 'y source': [1, 2, 3]})
    before = data.copy()
    _ = annotation.transform(data)
    pd.testing.assert_frame_equal(before, data)


def test_fillna():
    annotation = Annotation(
        """source_name,na_value,dtype
        x,0,int
        y,Nothing,string
        z,0.0,float
        """
    )
    data = pd.DataFrame({'x': [1, 2, 3, None], 'y': ['a', 'b', None, 'c'], 'z': [None, 1, 2, 3]})
    assert data.isna().any().any()
    transformed = annotation.transform(data)
    assert not transformed.isna().any().any()


def test_fillna_respect_type():
    annotation = Annotation(
        """source_name,na_value,dtype
        x,0,int
        y,Nothing,string
        z,0.0,float
        v,Low,category
        """
    )
    data = pd.DataFrame(
        {'x': [1, 2, 3, None], 'y': ['a', 'b', None, 'c'], 'z': [None, 1, 2, 3], 'v': ['Low', 'High', None, None]}
    )
    transformed = annotation.transform(data)
    for _, row in annotation.annotation.iterrows():
        col = row['name']
        dtype = row['dtype']
        if dtype == 'string':
            dtype = 'object'
        assert transformed.dtypes[col] == dtype


def test_fillna_resulting_values():
    annotation = Annotation(
        """source_name,na_value,dtype
        x,0,int
        y,Nothing,string
        z,0.0,float
        v,Low,category
        """
    )
    data = pd.DataFrame(
        {'x': [1, 2, 3, None], 'y': ['a', 'b', None, 'c'], 'z': [None, 1, 2, 3], 'v': ['Low', 'High', None, None]}
    )
    ref = {'x': {1, 2, 3, 0}, 'y': {'a', 'b', 'Nothing', 'c'}, 'z': {0, 1, 2, 3}, 'v': {'Low', 'High'}}
    transformed = annotation.transform(data)
    for col in transformed.columns:
        assert ref[col] == set(transformed[col])


def test_fillna_requires_dtype():
    annotation = Annotation(
        """source_name,na_value,dtype
        x,0,int
        y,Nothing,string
        z,0.0
        """
    )
    data = pd.DataFrame({'x': [1, 2, 3, None], 'y': ['a', 'b', None, 'c'], 'z': [None, 1, 2, 3]})
    with pytest.raises(ValueError):
        annotation.transform(data)


def _make_annotation_data_for_lowercase():
    annotation = make_dataframe("""
        source_name
        x
        """)
    data = pd.DataFrame(
        {'x': ['lower', 'UPPER', 'MixED']}
    )
    return annotation, data


def test_no_lowercase_by_default():
    annotation, data = _make_annotation_data_for_lowercase()
    annotation = Annotation(annotation)
    transformed = annotation.transform(data)
    pd.testing.assert_series_equal(data['x'], transformed['x'])


def test_no_lowercase_explicit():
    annotation, data = _make_annotation_data_for_lowercase()
    annotation = Annotation(annotation, lowercase_all_strings=False)
    transformed = annotation.transform(data)
    pd.testing.assert_series_equal(data['x'], transformed['x'])


def test_lowercase_explicit():
    annotation, data = _make_annotation_data_for_lowercase()
    annotation = Annotation(annotation, lowercase_all_strings=True)
    transformed = annotation.transform(data)
    pd.testing.assert_series_equal(data['x'].str.lower(), transformed['x'])

