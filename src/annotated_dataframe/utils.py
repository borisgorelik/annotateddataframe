import io
import numpy as np
import pandas as pd


def int_or_na(val):
    try:
        return int(val)
    except ValueError:
        return pd.NA


def float_or_na(val):
    try:
        return float(val)
    except TypeError:
        return None


def convert_values(values, converter, dtype=None):
    ret = pd.Series(map(converter, values), dtype=dtype)
    return ret


def df_from_string(s: str, dtype=None) -> pd.DataFrame:
    """Convert a CSV string to a dataframe."""
    df = pd.read_csv(io.StringIO(s.strip()), sep=' *, *', engine='python', dtype=dtype)
    return df


def make_dataframe(inp, fillna=None, dtypes=None) -> pd.DataFrame:
    """
    Make a `DataFrame` from the input `inp` which can be either a dataframe, its text representation, or a file name


    Parameters
    ----------
    inp : DataFrame or string
        We use the following heuristics:
         * if `inp` is a DataFrame, we return it as-is
         * if it's a string that contains at least two lines, we treat is as a CSV string
         * if it's a short string that ends with .csv or .xls or .xlsx, we treat it as a file name

    dtypes : dict, pd.Series or None
        If specified, and `inp` isn't already a DataFrame, we will use the information in this object to specify
        column dtypes during the reading time. This is useful when we read data with confusing types, such as
        IDs that are interpreted as integers, instead of strings

    fillna: str or None
        If specified, use this string to fill missing values of non-numeric columns

    Returns
    -------
    DataFrame
        The data frame

    """

    def _actual_make_dataframe(source, dtypes) -> pd.DataFrame:
        """Use heuristics to create a dataframe"""
        if isinstance(source, pd.DataFrame):
            return source
        if isinstance(source, str):
            if len(source.splitlines()) > 2:
                return df_from_string(source, dtype=dtypes)
            if source.endswith('.csv'):
                return pd.read_csv(source, engine='python', dtype=dtypes, quotechar='"')
            if source.endswith('.xls') or source.endswith('.xlsx'):
                return pd.read_excel(source, dtype=dtypes)

    if dtypes is not None:
        dtypes = dict(dtypes)
    ret = _actual_make_dataframe(inp, dtypes)
    for c in ret.columns:
        if ret[c].isna().all():
            ret[c] = pd.Series([''] * len(ret), dtype=str)
    if fillna is not None:
        numeric = ret.select_dtypes(np.number).columns
        for c in ret.columns:
            if c in numeric:
                continue
            ret[c].fillna(fillna, inplace=True)
    return ret


def eval_pd_formula(df: pd.DataFrame, formula: str) -> pd.Series:
    """USE WITH CAUTION!"""
    formula = formula.strip().strip('"')
    try:
        return df.eval(formula, engine='python')
    except:
        msg = f'Failed to eval formula "{formula}"'
        raise RuntimeError(msg)
