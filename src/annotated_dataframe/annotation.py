import warnings

import numpy as np
import pandas as pd

from annotated_dataframe import utils
from annotated_dataframe.utils import eval_pd_formula

TARGET = 'target'
FEATURE = 'feature'
META = 'meta'
SKIP = 'skip'
UNDEF_ROLE = ''
ROLES = {TARGET, FEATURE, META, SKIP, UNDEF_ROLE}

REQUIRED_COLUMNS = ['source_name']
SUPPORTED_COLUMNS = REQUIRED_COLUMNS + [
    'name', 'others_threshold_percent', 'note', 'dtype', 'role', 'formula', 'na_value'
]


def custom_asstring(val):
    try:
        int_val = int(val)
    except (TypeError, ValueError):
        return str(val)
    else:
        if int_val == val:
            return str(int_val)
        else:
            return str(val)


class Annotation:
    def __init__(self, annotation, strip_strings=True, lowercase_all_strings=False):
        annotation = utils.make_dataframe(annotation, fillna='')
        annotation = self._process_annotation(annotation)
        self.annotation = annotation
        self.strip_strings = strip_strings
        self.lowercase_all_strings = lowercase_all_strings

    def iterrows(self):
        return self.annotation.iterrows()

    def __repr__(self):
        return repr(self.annotation)

    def __str__(self):
        return str(self.annotation)

    def __getitem__(self, item):
        try:
            self.annotation.loc[item]
        except KeyError as err:
            if item in self.annotation.columns:
                return self.annotation[item]
            else:
                raise err

    def __getattr__(self, item):
        if item in self.annotation.columns:
            return self.annotation[item]
        else:
            raise AttributeError(f"Annotation object has no attribute '{item}'")

    def find_by_source_name(self, source_name):
        ret = self.annotation.loc[self.annotation.source_name == source_name]
        if ret.empty:
            raise LookupError(f"Annotation doesn't contain variables belonging with source name '{source_name}'")

    @classmethod
    def _process_annotation(cls, annotation) -> pd.DataFrame:
        errs = []
        for c in REQUIRED_COLUMNS:
            if c not in annotation:
                errs.append(f"Missing required column '{c}'")
        if errs:
            raise ValueError('; '.join(errs))
        for c in annotation.columns:
            if c not in SUPPORTED_COLUMNS:
                warnings.warn(f"Column '{c}' is not supported in annotation", RuntimeWarning)

        for c in SUPPORTED_COLUMNS:
            if c not in annotation:
                annotation[c] = ''
        annotation['source_name'] = annotation.source_name.fillna('')
        annotation['name'] = annotation['name'].fillna('')
        annotation = cls._process_annotation_formula(annotation)
        annotation = cls._process_annotation_roles(annotation)
        annotation = cls._process_threshold(annotation)
        if not annotation['name'].fillna('').str.strip().astype(bool).all():
            raise ValueError("Column `name` is not allowed to contain empty strings")
        annotation = annotation.set_index('name', drop=False)
        return annotation

    @staticmethod
    def _process_threshold(annotation: pd.DataFrame) -> pd.DataFrame:
        thresholds = annotation['others_threshold_percent']
        if (thresholds == '').all():
            annotation['others_threshold_percent'] = pd.Series([None] * len(thresholds), dtype=float)
            assert 'others_threshold_percent' in annotation.select_dtypes(np.number)
        else:
            thresholds = thresholds[~pd.isna(thresholds)]
            try:
                thresholds = thresholds.astype(float)
            except ValueError:
                raise ValueError(
                    'Error while processing the `others_threshold_percent` column. '
                    'Only numeric and empty cells are allowed'
                )
            dtp = str(thresholds.dtype).lower()
            if (dtp == 'o') or ('str' in dtp):
                raise ValueError("'others_threshold_percent' columns can only have numbers or empty cells")
            if len(thresholds):
                msgs = []
                if (thresholds < 0).any():
                    msgs.append('Negative threshold values are not allowed')
                if (thresholds > 100).any():
                    msgs.append('Threshold values above 100% are not allowed')
                if msgs:
                    raise ValueError(
                        'Error while processing the `others_threshold_percent` column: ' '\n'.join(msgs)
                    )
        return annotation

    @staticmethod
    def _process_annotation_roles(annotation):
        existing_roles = set(annotation.role)
        role_diff = existing_roles.difference(set(ROLES))
        if role_diff:
            str_supported = str(ROLES)
            role = 'Role' if len(role_diff) == 1 else 'Roles'
            is_are = 'is' if len(role_diff) == 1 else 'are'
            str_unsupported = str(role_diff)
            msg = f'Data annotation only supports the following roles {str_supported}. ' \
                  f'{role} {str_unsupported} {is_are} not supported'
            raise ValueError(msg)
        return annotation

    @staticmethod
    def _process_annotation_formula(annotation: pd.DataFrame) -> pd.DataFrame:
        msgs = []
        for ix, row in annotation.iterrows():
            if row['source_name'] != '':
                # if `source_name` exists, make sure `formula` doesn't, and set `name` to `source_name`
                if row['formula'] != '':
                    msgs.append(
                        f"Variable '{row['source_name']}' defines both `source_name` and `formula` which is ambigous'"
                    )
                elif row['name'] == '':
                    # `row['name'] = xxx` results in warnings
                    annotation.loc[ix, 'name'] = row['source_name']
            elif row['formula'] == '':
                # if `source_name` doesn't exist, make sure `formula` does
                msgs.append(
                    'Annotation was found without a `source_name` and without a `formula`'
                )
        for col in ('name', 'source_name'):
            vals = annotation[col]
            vals = vals[vals != '']
            if vals.nunique() != len(vals):
                cnts = pd.value_counts(vals)
                cnts = cnts[cnts > 1]
                msgs.append(f"Annotation column '{col}' isn't unique: {cnts.to_dict()}")
        if msgs:
            raise ValueError('; '.join(msgs))

        # A row can only have a source_name or formula, but not both
        sel_rows_with_source_name = annotation['source_name'] != ''
        formulas = annotation.loc[sel_rows_with_source_name]['formula']
        if not (formulas == '').all():
            raise ValueError(f'Annotation rows with `source_name`s and `formula` are ambiguous')
        return annotation

    def dtypes(self, source_names=False) -> pd.Series:
        """
        Return variable dtypes

        Parameters
        ----------
        source_names : bool
            If `True` will return dypes specified for the source variable names, otherwise, the types
            will use the transformed names

        Returns
        -------
        pd.Series:
            key-value pairs of name-type
        """
        if source_names:
            ret = self.annotation[['source_name', 'dtype']].set_index('source_name', drop=True)['dtype']
        else:
            ret = self.annotation['dtype']
        ret = ret.loc[ret != '']
        return ret

    def transform(self, data: pd.DataFrame) -> pd.DataFrame:
        data = data.copy()
        sel_has_formula = self.annotation.formula != ''
        for _, row in self.annotation.loc[~sel_has_formula].iterrows():
            # First rename columns, transfer dtypes, then compute formulas
            source_name = row['source_name']
            name = row.name
            na_value = row.na_value
            if not (pd.isna(row.na_value) or (row.na_value == '')):
                required_type = row['dtype']
                if required_type == '':
                    msg = f"Variable with source name {source_name} defined an na_value but not dtype. " \
                          f"This isn't allowed"
                    raise ValueError(msg)
                if data[source_name].isna().any():
                    data[source_name] = data[source_name].fillna(na_value).astype(required_type)
            if source_name not in data:
                raise ValueError(f"Data doesn't contain column '{source_name}'")
            if source_name == name:
                continue
            if name in data:
                raise ValueError(
                    f"Source data already contains column named '{name}'; "
                    f"can't create another column with this name"
                )
            data.rename(columns={source_name: name}, inplace=True)
        if 'dtype' in self.annotation:
            sel = self.annotation.dtype.astype(bool)
            for _, row in self.annotation[sel].iterrows():
                column_name = row.name
                row_formula = row.formula
                if row_formula:
                    continue
                else:
                    required_type = row['dtype']
                    data[column_name] = self._handle_dtype(data[column_name], required_type)
        for _, row in self.annotation.loc[sel_has_formula].iterrows():
            name = row['name']
            formula = row['formula']
            data[name] = self._handle_dtype(eval_pd_formula(data, formula), row['dtype'])
        if self.strip_strings or self.lowercase_all_strings:
            for col, typ in data.dtypes.iteritems():
                if typ == 'object':
                    if self.strip_strings:
                        data[col] = data[col].str.strip()
                    if self.lowercase_all_strings:
                        data[col] = data[col].str.lower()
        return data

    def _handle_dtype(self, values: pd.Series, required_type: str) -> pd.Series:
        if pd.isna(required_type) or not bool(required_type):
            return values
        if required_type in {'str', 'string'}:
            values = values.apply(custom_asstring)
            # otherwise, numbers don't convert to strings
        else:
            if 'int' in required_type.lower():
                try: # DEBUG
                    values = utils.convert_values(  # much more robust than .astype
                        values, converter=utils.int_or_na, dtype='int'
                    )
                except TypeError:
                    raise TypeError()
            elif 'float' in required_type.lower():
                values = utils.convert_values(  # much more robust than .astype
                    values, converter=utils.float_or_na, dtype='float'
                )
            elif 'datetime' in required_type.lower():
                values = pd.to_datetime(values)
            elif required_type == 'category':
                values = values.astype('category')
            else:
                msg = f"dtype '{required_type}' isn't supported yet"
                raise NotImplementedError(msg)
        return values
