from typing import Iterable, Union, List

import pandas as pd

from annotated_dataframe import utils
from annotated_dataframe.annotation import Annotation
import annotated_dataframe.annotation as df_annotation


class AnnotatedDataFrame(pd.DataFrame):
    _metadata = ['_annotation']

    def __init__(self, data, annotation):
        if not isinstance(annotation, Annotation):
            annotation = Annotation(utils.make_dataframe(annotation))
        self._annotation = annotation
        dtypes = self.annotation.dtypes()
        if dtypes.empty:
            dtypes = None
        data = utils.make_dataframe(data, dtypes)
        data = self.annotation.transform(data)
        super(AnnotatedDataFrame, self).__init__(data)

    @property
    def _constructor(self):
        if 'annotation' in self.__dict__:
            return AnnotatedDataFrame
        else:
            return pd.DataFrame

    @property
    def annotation(self):
        return self._annotation

    def _process_data(self, data):
        return pd.DataFrame(data)

    def __getattr__(self, item)->pd.DataFrame:
        supported_roles = [r for r in df_annotation.ROLES if r] + ['undef_role']
        if item in supported_roles:
            return self.get_data_by_role(item)
        elif item in self.columns:
            return pd.DataFrame(self)[item]
        else:
            raise AttributeError(f"'AnnotatedDataFrame' object has no attribute '{item}")

    def get_data_by_role(self, role: str)->pd.DataFrame:
        cols_to_take = self.column_names_from_roles(role)
        ret = pd.DataFrame(self)[cols_to_take]
        return ret


    def column_names_from_roles(self, roles: Union[str, Iterable]) -> List[str]:
        """Return column names by their annotated roles"""
        if isinstance(roles, str):
            roles = [roles]
        ret = []
        for role in roles:
            if role == 'undef_role':
                role = df_annotation.UNDEF_ROLE
            assert role in df_annotation.ROLES
            cols_to_take = self.annotation.annotation.loc[self.annotation.role == role]['name'].values
            ret.extend(cols_to_take)
        return ret

    def trimmed(self, including_undef=True)->pd.DataFrame:
        """
        Return a trimmed copy of the data.

        Only return the columns that were not marked in annotation as "skip". If `including_undef` is True, treat
        the columns without a defined role as "skip"

        Parameters
        ----------
        including_undef : bool
            Should we treat the columns without a defined role as "skip"?
        Returns
        -------
        pd.DataFrame
        A trimmed, NON-ANNOTATED copy of the data
        """

        roles_to_skip = {'skip'}
        if including_undef:
            roles_to_skip.add(df_annotation.UNDEF_ROLE)
        roles_to_keep = [r for r in df_annotation.ROLES if r not in roles_to_skip]
        cols_to_keep = self.column_names_from_roles(roles_to_keep)
        return pd.DataFrame(self)[cols_to_keep]
